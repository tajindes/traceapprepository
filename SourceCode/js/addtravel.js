﻿function check() {
    var arr = document.getElementById("dateArrival").value;
    var dep = document.getElementById("dateDeparture").value;
    var location = document.getElementById("txtLocation").value;
    var pattern = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var regex = /^[a-zA-Z]+$/;

    if (arr.trim() == "") {
        alert("Please enter arrival date");
        arr.focus();
        return false;
    }

    if (!pattern.test(arr)) {
        alert("Please enter valid arrival date");
        arr.focus();
        return false;

    }

    if (dep.trim() == "") {
        alert("Please enter departure date");
        dep.focus();
        return false;
    }

    if (!pattern.test(dep)) {
        alert("Please enter valid departure date");
        dep.focus();
        return false;

    }

    if (location.trim() == "") {
        alert("Please enter location");
        location.focus();
        return false;
    }

		var userId = window.localStorage.getItem("localStorage_uId");
		var userName = window.localStorage.getItem("localStorage_userName");

/*		var arr = document.getElementById("dateArrival").value;
		var dep = document.getElementById("dateDeparture").value;
		var location = document.getElementById("txtLocation").value;
*/

		var separate = location.split(",");
		var newcity = separate[separate.length - 3];
var RegObect =
{
	"userId": userId,       
	"userName": userName,
    "toCity": newcity,
    "fromDate": arr,
    "toDate": dep,
    "type": "travel"
               
};

    $.ajax(
    {
        type: 'POST',
        url: 'http://tracebackend-ts.azurewebsites.net/api/Acc/AddNewTravel',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(RegObect)
    }).done(function (data) {
		 if(data.errorCode == 0){
			alert("Travel added successfully");
			window.location = "HomePage.html";
		 }
		 else if (data.errorCode == 1) {
			alert("FAILED and error message is: " + data.errorMessage);
		 }
		 else {
			alert("some other error message. debug it !!!");
		}

    }).fail(function (error) {
        alert('Some other http error');
        alert(error.toString());
    });

}

