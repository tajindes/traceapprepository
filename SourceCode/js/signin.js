﻿$(document).ready(function () {
    $("body").hide().fadeIn(1250);
});

function check() {
    var email = document.getElementById("txtEmail").value;
    var password = document.getElementById("txtPwd").value;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var regex = /^[a-zA-Z]+$/;

    if (email.trim() == "") {
        alert("Please enter email id");
        email.focus();
        return false;
    }

    if (!re.test(email)) {
        alert("Please enter valid email id");
        email.focus();
        return false;

    }

    if (password.trim() == "") {
        alert("Please enter password");
        password.focus();
        return false;
    }

    if (password.trim().length < 6) {
        alert("Please enter password with atleast 6 characters");
        password.focus();
        return false;
    }

    // Make AJAX call here
    var loginData = {
        grant_type: 'password',
        username: document.getElementById("txtEmail").value,
        password: document.getElementById("txtPwd").value
    };

    $.ajax({
        type: 'POST',
        url: '/Token',
        data: loginData
    }).done(function (data) {
        // self.user(data.userName);
        // Cache the access token in session storage.
        // sessionStorage.setItem(tokenKey, data.access_token);
        window.localStorage.setItem("tokenKey", data.access_token);
        window.localStorage.setItem("tokenType", data.token_type);
        window.localStorage.setItem("tokenExpiresIn", "data.expires_in");
        window.localStorage.setItem("tokenUserName", "data.userName");
        window.localStorage.setItem("tokenIssue", "data.issued");
        window.localStorage.setItem("tokenExpires", "data.expires");

    }).fail(alert('POST failed'));
}

function SignUp1() {
		window.location = "SignUp1.html";
}

function forgotPassword() {
		window.location = "ForgotPassword.html";
}
