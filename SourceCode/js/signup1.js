﻿$(document).ready(function () {
    $("body").hide().fadeIn(1250);
});

function check() {
    var fname = document.getElementById("txtFname").value;
    var lname = document.getElementById("txtLname").value;
    var email = document.getElementById("txtEmail").value;
    var password = document.getElementById("txtPwd").value;
    //var city = document.getElementById("txtHomeCity").value;
    var commcode = document.getElementById("txtCommCode").value;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var regex = /^[a-zA-Z]+$/;

    

    if (fname.trim() == "") {
        alert("Please enter first name");
        fname.focus();
        return false;
    }

    if (!regex.test(fname)) {
        alert("Please enter valid first name");
        fname.focus();
        return false;

    }

    if (lname.trim() == "") {
        alert("Please enter last name");
        lname.focus();
        return false;
    }

    if (!regex.test(lname)) {
        alert("Please enter valid last name");
        lname.focus();
        return false;

    }

    if (email.trim() == "") {
        alert("Please enter email id");
        email.focus();
        return false;
    }

    if (!re.test(email)) {
        alert("Please enter valid email id");
     //   email.focus();
        return false;

    }

    if (password.trim() == "") {
        alert("Please enter password");
        password.focus();
        return false;
    }

    if (password.trim().length < 6) {
        alert("Please enter password with atleast 6 characters");
        password.focus();
        return false;
    }

    if (commcode.trim() == "") {
        alert("Please enter Community Code");
        commcode.focus();
        return false;
    }
    else {
        window.location = "../SignUp2.html";
    }
}
function GoBack() {
    window.location = "../SignIn.html";
}