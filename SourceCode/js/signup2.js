﻿function check() {

    var city = document.getElementById("txtHomeCity").value;

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var regex = /^[a-zA-Z]+$/;


    if (city.trim() == "") {
        alert("Please enter city");
        city.focus();
        return false;
    }

    if (!regex.test(city)) {
        alert("Please enter valid city");
        city.focus();
        return false;

    }

}

function initialize() {
    var input = /** @type {HTMLInputElement} */(
        document.getElementById('txtHomeCity'));

    var autocomplete = new google.maps.places.Autocomplete(input);

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        //  alert("city is:" + place.name);
        // alert("formatted address is:" + place.formatted_address);
        if (!place.geometry) {
            return;
        }
        var address = '';
        if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
            //     alert("address is:" + address);
        }
    });

}

google.maps.event.addDomListener(window, 'load', initialize);

function getPhoto(source) {
    // Retrieve image file location from specified source
    navigator.camera.getPicture(onPhotoURISuccess, onFail, {
        quality: 50,
        destinationType: destinationType.FILE_URI,
        sourceType: source
    });
}

function onFail(message) {
    alert('Failed because: ' + message);
}


function onPhotoURISuccess(imageURI) {
    // Uncomment to view the image file URI
    // console.log(imageURI);

    // Get image handle
    //
    var largeImage = document.getElementById('largeImage');

    // Unhide image elements
    //
    largeImage.style.display = 'block';

    // Show the captured photo
    // The in-line CSS rules are used to resize the image
    //
    largeImage.src = imageURI;
}
function GoBack() {
    window.location = "../SignIn.html";
}