﻿function Validate() {
    var email = document.getElementById("txtEmail").value;

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var regex = /^[a-zA-Z]+$/;

    if (email.trim() == "") {
        alert("Please enter email id");
        email.focus();
        return false;
    }

    if (!re.test(email)) {
        alert("Please enter valid email id");
        email.focus();
        return false;
    }    
}

function SignIn() {
    window.location = "SignIn.html";
}