﻿function check() {
    var arr = document.getElementById("dateArrival").value;
    var dep = document.getElementById("dateDeparture").value;
    var location = document.getElementById("txtLocation").value;
    var pattern = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var regex = /^[a-zA-Z]+$/;

    if (arr.trim() == "") {
        alert("Please enter arrival date");
        arr.focus();
        return false;
    }

    if (!pattern.test(arr)) {
        alert("Please enter valid arrival date");
        arr.focus();
        return false;

    }

    if (dep.trim() == "") {
        alert("Please enter departure date");
        dep.focus();
        return false;
    }

    if (!pattern.test(dep)) {
        alert("Please enter valid departure date");
        dep.focus();
        return false;
    }

    if (location.trim() == "") {
        alert("Please enter location");
        location.focus();
        return false;
    }

		var userId = window.localStorage.getItem("localStorage_uId");
		var userName = window.localStorage.getItem("localStorage_userName");
		var travelId = window.localStorage.getItem("editTravelId");

		var separate = location.split(",");
		var newcity = separate[separate.length - 3];


/*		var arr = document.getElementById("dateArrival").value;
		var dep = document.getElementById("dateDeparture").value;
		var location = document.getElementById("txtLocation").value;
*/

var RegObect =
{
	"userId": userId,       
	"userName": userName,
    "toCity": newcity,
    "fromDate": arr,
    "toDate": dep,
    "type": "travel",
    "TravelId": travelId//"1"            
};

    $.ajax(
    {
        type: 'POST',
        url: 'http://tracebackend-ts.azurewebsites.net/api/Acc/UpdateTravel',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(RegObect)
    }).done(function (data) {
		 if(data.ErrorCount == 0){
			alert("success and Travel Updated");
		 }		 
		 else {
			alert("Travel Not Updated !! debug it !!!");
		}
		window.location="HomePage.html";

    }).fail(function (error) {
        alert('Some other http error');
        alert(error.toString());
    });

}

